# Analyse et Traitement Images

Ceci est le repository Gitlab du rendu de la matière *Analyse et Traitement Images*.

Il se décompose de la façon suivante :

    ├── data/                       # Base de donnée de 10 800 images divisées en catégories
    ├── NewMethod (NeuralNetwork)/  # Réseau de neurones pour le traitement d'images
    ├── Old Method/        		    # Vecteurs d'attributs pour le traitement d'images
    ├── Rapport_Traitement_Images_AUDEMARD_Aymeric. pdf 	# Rapport 
    └── README.md       		    # Informations complémentaires


De plus, voici plus de précisions par rapport au dossier *NewMethod (NeuralNetwork)/* :

    ├── data/	    # Lien symbolique vers le dossier data/ présent à la racine
    ├── dataset/	# Jeux d'images de train/validation/test, résultats et classes utilisées
    └── src/	    # Différents fichiers python, graphiques de performances et le modèle

Voici maintenant plus de précisions par rapport au dossier *Old Method/* :

    ├── data/	    # Lien symbolique vers le dossier data/ présent à la racine
    ├── dataset/	# Dossier contenant les jeux d'images de train, validation 
    ├── result/	    # Images triées par un programme python comme color.py
    ├── src/	    # Dossier contenant les différents fichiers python
    ├── README.md	# Fichier écrit par l'utilisateur Github pochih
    └── USAGE.md	# Fichier lui aussi écrit par l'utilisateur Github pochih

<b><u> /!\ </u> IMPORTANT <u> /!\ </u></b><hr>
Veillez à bien supprimer, entre chaque utilisation des programmes python, les dossier cache et _pycache_ dans src/, ainsi que train.csv et validation.csv 
